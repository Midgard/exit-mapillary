#!/usr/bin/env python3

import sys
import urllib.request
import urllib.parse
import json
import re
from functools import partial


CHUNK_SIZE = 1000


API = "https://a.mapillary.com/v3"
OAUTH_ENDPOINT = "https://www.mapillary.com/connect"

# The redirect URL must match the configuration of the OAuth registration. So if you change this,
# you will have to register for a new client ID
OAUTH_REDIRECT = "http://localhost:3817/"  # Not expected to be listened on

OAUTH_CLIENT_ID = "TzcwbF9PMlR0Ynl0S0IzQTBtcWN2dzoyZWIyMjA2NzE0NDYwYWNl"
OAUTH_SCOPE = ["public:write", "private:read", "private:write"]


def urlencode(s, safe=""):
	return urllib.parse.quote(s, safe)


# To have zero dependencies, we write our own "HTTP library"
def request(url, method="GET", url_params={}, json_data=None, access_token=None):
	headers = {
		"Accept":        "application/json",
		"User-Agent":    "Exit Mapillary/0.1"
	}
	if access_token:
		headers["Authorization"] = "Bearer " + urlencode(access_token)

	data = None
	if json_data is not None:
		data = json.dumps(json_data).encode()
		headers["Content-Type"] = "application/json"

	first = True
	all_url_params = {
		"client_id": OAUTH_CLIENT_ID,
		**url_params
	}
	for k, v in all_url_params.items():
		url += ("?" if first else "&") + urlencode(k) + "=" + urlencode(v)
		first = False

	req = urllib.request.Request(url, method=method, data=data, headers=headers)

	with urllib.request.urlopen(req) as resp:
		body = resp.read()

	return json.loads(body)


def sequences(username, http, also_private=True):
	def get_sequences(private):
		private_str = "true" if private else "false"
		r = http(API + "/sequences", url_params={"usernames": username, "private": private_str})
		return r["features"]

	yield from get_sequences(private=False)
	try:
		if also_private:
			yield from get_sequences(private=True)
	except:
		print("Could not read private photos, deleting only public ones.")


def image_keys_in_sequences(sequences_data):
	for feature in sequences_data:
		yield from feature["properties"]["coordinateProperties"]["image_keys"]


def delete(img_keys, http):
	data = {
		"type": "deletion",
		"changes": [
			{"image_key": img_key}
			for img_key in img_keys
		]
	}
	try:
		return http(API + "/changesets", method="POST", json_data=data)
	except urllib.error.HTTPError as e:
		body = e.read().decode()
		if body == '{"message":"None of changes is valid. Hence no changeset was created"}':
			print("Server said 'None of changes is valid. Hence no changeset was created'. Probably all image deletions in this chunk are already pending")
		else:
			print("{} {} for POST {}. Response:".format(e.code, e.reason, e.url))
			print(body)
			print()
			raise e


def chunks(chunk_size, sequence):
	return [
		sequence[i:i+chunk_size]
		for i in range(0, len(sequence), chunk_size)
	]


def show_changeset(changeset):
	try:
		print("Created {} changeset {}".format(changeset["type"], changeset["key"]))

	except KeyError:
		print(json.dumps(changeset))
		print("Error: Response was not recognized as changeset object, it might not have worked!")


def main():
	print("Exit Mapillary")
	print("--------------")
	print("This program will submit all your Mapillary photos for deletion.")

	redirect_uri = urlencode(OAUTH_REDIRECT)
	scope = urlencode(" ".join(OAUTH_SCOPE))
	authorization_url = OAUTH_ENDPOINT + "?scope=" + scope + "&client_id=" + OAUTH_CLIENT_ID + \
		"&redirect_uri=" + redirect_uri + "&state=return&response_type=token"

	# Strangely, Mapillary does not allow applications to see the user details of the *currently
	# logged in user* you are. Or it's hidden very well in the docs. We hack this by asking the
	# user.
	username = input("Your Mapillary username: ")

	print()
	print()
	print("To continue, open this URL in a web browser:")
	print(authorization_url)
	print()
	print("Your email address is not used in this program. Mapillary always claims applications "
		"\"want to read your email address\" for some weird reason.")
	print()
	print("After you log in, you'll be redirected to " + OAUTH_REDIRECT + "?blablabla.")
	print("==================================")
	print("===  That page will not load.  ===")
	print("==================================")
	authorization_response = input("Instead, copy the URL and paste it here: ")
	print()
	print()

	m = re.search(r"access_token=([^&]+)", authorization_response)
	if not m:
		raise ValueError("Couldn't find access token in URL")
	access_token = urllib.parse.unquote(m.group(1))

	authorized_request = partial(request, access_token=access_token)

	print("Collecting images...")
	seq_data = sequences(username, authorized_request)
	imgs = list(image_keys_in_sequences(seq_data))
	print("Discovered " + str(len(imgs)) + " images")

	chunked_imgs = chunks(CHUNK_SIZE, imgs)

	for i, chunk in enumerate(chunked_imgs):
		print("Chunk {} of {}: requesting deletion of {} images... ".format(
			i + 1, len(chunked_imgs), len(chunk)
		), end="")
		deletion_cs = delete(chunk, authorized_request)
		if deletion_cs:
			show_changeset(deletion_cs)

	print("Done. Once the deletion request has been processed, you will see this in your "+
		"feed on the Mapillary website. You might also receive an email from Mapillary, " +
		"but this doesn't seem to work reliably.")
	return True

if __name__ == '__main__':
	sys.exit(0 if main() else 1)
