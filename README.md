# Exit Mapillary

Mapillary sold themselves to Facebook. Time to abandon ship.

*Exit Mapillary* allows you to easily remove all photos from your Mapillary account to try and give
Facebook as little data as possible. When your photos have been deleted, you can delete your
account (and/or send a GDPR deletion request if you're from the EU).

Before you delete your pictures, you can download them with
[**mapillary_takeout**](https://github.com/gitouche-sur-osm/mapillary_takeout) by gitouche-sur-osm.
You will need a bit more technical background to use mapillary_takeout than to use Exit Mapillary.
Ask a friend to help!

## Get started

### Windows
1. This tool requires Python to be installed on your device. You might need to [install Python first](https://www.python.org/downloads/).

2. Open the command prompt: in Start, search for `cmd`.

3. Run Exit Mapillary by copying `curl -Ls https://framagit.org/Midgard/exit-mapillary/-/raw/master/exit.py > exit-mapillary.py && python exit-mapillary.py & del exit-mapillary.py`, pasting it in the command prompt (right click in the window) and pressing enter.

### macOS
1. Open the Terminal app.

2. Run Exit Mapillary by copying `curl -Ls https://framagit.org/Midgard/exit-mapillary/-/raw/master/exit.py > exit-mapillary.py && python3 exit-mapillary.py ; rm exit-mapillary.py`, pasting it in the terminal and pressing enter.


### Other
1. Open a terminal window.

2. Run Exit Mapillary by copying `wget -qO- https://framagit.org/Midgard/exit-mapillary/-/raw/master/exit.py > exit-mapillary.py && python3 exit-mapillary.py ; rm exit-mapillary.py`, pasting it in the terminal* and pressing enter.

\*`Ctrl+V` doesn't work in the terminal. Use the toolbar, the right-click menu, or try `Ctrl+Shift+V`.

## What it looks like
```
$ python exit.py
Exit Mapillary
--------------
This program will submit all your Mapillary photos for deletion.
Your Mapillary username: <you enter your username here>


To continue, open this URL in a web browser:
https://www.mapillary.com/connect?scope=public%3Awrite%20private%3Aread%20private%3Awrite&client_id=TzcwbF9PMlR0Ynl0S0IzQTBtcWN2dzoyZWIyMjA2NzE0NDYwYWNl&redirect_uri=http%3A%2F%2Flocalhost%3A3817%2F&state=return&response_type=token

Your email address is not used in this program. Mapillary always claims applications "want to read your email address" for some weird reason.

After you log in, you'll be redirected to http://localhost:3817/?blablabla.
==================================
===  That page will not load.  ===
==================================
Instead, copy the URL and paste it here: <you paste the URL here>


Collecting images...
Discovered 6181 images
Chunk 1 of 7: requesting deletion of 1000 images... Created deletion changeset DitTerqr2HQNTH40pV6yg9
Chunk 2 of 7: requesting deletion of 1000 images... Created deletion changeset hjxpzbw9mLgYW5Lf6TCos0
Chunk 3 of 7: requesting deletion of 1000 images... Created deletion changeset 41nEEyjix4wgPdfEHrNJRC
Chunk 4 of 7: requesting deletion of 1000 images... Created deletion changeset RoWi6NAsQXz95obEzJLMHb
Chunk 5 of 7: requesting deletion of 1000 images... Created deletion changeset G2f6hhnrx019bLhTXEh6nM
Chunk 6 of 7: requesting deletion of 1000 images... Created deletion changeset unVoDsQI5hRJ4EVph0T9Vi
Chunk 7 of 7: requesting deletion of 181 images... Created deletion changeset XJntsVCcLLMqoyMn2i03n8
Done. Once the deletion request has been processed, you will see this in your feed on the Mapillary website. You might also receive an email from Mapillary, but this doesn't seem to work reliably.
If the changesets don't show up in a few days, you can try again.
$ 
```

![Mapillary website. It shows "Pending edits: 1" on top. Below there are two items "Your 2 requested edits have been approved": from 22 and 19 June 2020](mapillary_feed.png)
